var _speed = argument0;

if (os_type == os_win32 || os_type == os_browser) {
    distance_to_mouse = distance_to_point(mouse_x, mouse_y);
    if (distance_to_mouse > 3) {
        real_speed = _speed;
        if (
            (y < mouse_y && y + sprite_height/2 >= view_hview - global.room_bottom_margin)
            || (y > mouse_y && y - sprite_height/2 + sprite_height <= global.room_top_margin)
            || (x < mouse_x && x + (image_xscale * sprite_width/2) >= view_wview)
            || (x > mouse_x && x - (image_xscale * sprite_width/2) <= 0) 
            )
        {
            real_speed = 0;
        }
        move_towards_point(mouse_x, mouse_y, real_speed * global.timeScale);
    } else {
        real_speed = 0;
    }
}
