/// approach(start, ending, difference);
var _start, ending, difference;

_start = argument0;
ending = argument1;
difference = argument2;

var result;

if (_start < ending){
    result = min(_start + difference, ending); 
}else{
    result = max(_start - difference, ending);
}

return result;
