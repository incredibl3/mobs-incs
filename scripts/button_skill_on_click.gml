var skill_number;
if (argument0 > 0)
    skill_number = argument0;
else 
    skill_number = action;

var ok = false;
for (i = 0; i < instance_number(obj_skill_button); i += 1)
{
   inst = instance_find(obj_skill_button,i);
   if (inst.action == skill_number && (inst.state == SKILL_UNLOCK || inst.state == SKILL_INUSE)) {
        ok = true;
   }
}

if ((obj_mc.state != "pre_use_skill" && obj_mc.state != "wait_use_skill" && obj_mc.state != "use_skill" 
    && obj_mc.state != "dead" && obj_mc.is_mc_dead <= 0 && (state == SKILL_INUSE || state == SKILL_UNLOCK)) && ok) {
    obj_mc.mc_is_using_skill = skill_number;
    obj_mc.state = "pre_use_skill";
    
    if (skill_number == 2)
        obj_mc.skill_start_time_stamp = current_time;
}
