/// list_contain_instance_id(id, id_to_check)
//
//  Returns true if the list contains the id, false otherwise
//
//      id              	The id of the list to use.
//      id_to_check     The id of the instance to be checked
//
{
    var list, id_to_check, temp;
    list = argument0;
    id_to_check = argument1;
    
    for (i = 0; i < ds_list_size(list); i++) {
        temp = list[| i];
        if (temp == id_to_check)
            return true;
    }
    
    return false;
}
