if (
        (y + sprite_height/2 >= view_hview - global.room_bottom_margin)
        || (y - sprite_height/2 <= global.room_top_margin)
        || (x + (image_xscale * sprite_width/2) >= view_wview)
        || (x - (image_xscale * sprite_width/2) <= 0
        || real_speed != argument0 )   // This is a hack, supposed that the speed when using skill 2 always greater than movement speed
        )
    {
        real_speed = argument0;
        move_towards_point(irandom(1) * view_wview, irandom(1) * view_hview, real_speed * global.timeScale);
    }
