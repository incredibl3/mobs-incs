/// GET RANDOM TEXT

var len = array_length_1d(argument0);
var text_index = irandom_range(0, len-1);
LOGD("@@@ Random index = " + string(text_index));
return string(argument0[text_index]);

