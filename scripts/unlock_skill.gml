var skill_action = argument0;

for (i = 0; i < instance_number(obj_skill_button); i += 1)
{
   inst = instance_find(obj_skill_button,i);
   if (inst.action == skill_action) {
    inst.state = SKILL_UNLOCK;
    break;
   }
}
