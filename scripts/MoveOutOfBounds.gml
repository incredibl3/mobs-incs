/// Check if the object is out of bound


if (x + abs(sprite_width) /2 > view_wview) {
    speed = 0;
} else if (x - abs(sprite_width)/2 < 0) {
    speed = 0;
} else if (y + sprite_height/2 > view_hview - global.room_bottom_margin) {
    speed = 0;
} else if (y - sprite_height/2 < 0 + global.room_top_margin) {
    speed = 0;
}
