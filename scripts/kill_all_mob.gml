var mc = id; 
var silent = argument0;
var inst;


for (i = 0; i < instance_number(obj_green_arrow); i += 1)
{
    inst = instance_find(obj_green_arrow, i);
    with(inst) {
        instance_destroy();
        if (silent == false) {
            PlayGFX(gfx_kill_mobs);
            obj_controller.number_of_mobs--;
            mc_add_xp_when_kill_mob(mc, "obj_green_arrow");                               
        }
    }
}

for (i = 0; i < instance_number(obj_gray_arrow); i += 1)
{
    inst = instance_find(obj_gray_arrow, i);
    with(inst) {
        instance_destroy();
        if (silent == false) {
            PlayGFX(gfx_kill_mobs);
            obj_controller.number_of_mobs--;
            mc_add_xp_when_kill_mob(mc, "obj_gray_arrow");                               
        }
    }
}

for (i = 0; i < instance_number(obj_swordsman); i += 1)
{
    inst = instance_find(obj_swordsman, i);
    with(inst) {
        instance_destroy();
        if (silent == false) {
            PlayGFX(gfx_kill_mobs);
            obj_controller.number_of_mobs--;
            mc_add_xp_when_kill_mob(mc, "obj_swordsman");                               
        }
    }
}

for (i = 0; i < instance_number(obj_long_swordsman); i += 1)
{
    inst = instance_find(obj_long_swordsman, i);
    with(inst) {
        instance_destroy();
        if (silent == false) {
            PlayGFX(gfx_kill_mobs);
            obj_controller.number_of_mobs--;
            mc_add_xp_when_kill_mob(mc, "obj_long_swordsman");                               
        }
    }
}

for (i = 0; i < instance_number(obj_running_man); i += 1)
{
    inst = instance_find(obj_running_man, i);
    with(inst) {
        instance_destroy();
        if (silent == false) {
            PlayGFX(gfx_kill_mobs);
            obj_controller.number_of_mobs--;
            mc_add_xp_when_kill_mob(mc, "obj_running_man");
        }                            
    }
}

for (i = 0; i < instance_number(obj_armor_man); i += 1)
{
    inst = instance_find(obj_armor_man, i);
    with(inst) {
        instance_destroy();
        if (silent == false) {
            PlayGFX(gfx_kill_mobs);
            obj_controller.number_of_mobs--;
            mc_add_xp_when_kill_mob(mc, "obj_armor_man");                               
        }
    }
}

if (silent == true) {
    for (i = 0; i < instance_number(obj_arrow); i += 1)
    {
        inst = instance_find(obj_arrow, i);
        with(inst) {
            instance_destroy();
        }
    }
    for (i = 0; i < instance_number(obj_arrow_gray); i += 1)
    {
        inst = instance_find(obj_arrow_gray, i);
        with(inst) {
            instance_destroy();
        }
    }
    for (i = 0; i < instance_number(obj_slash_anim); i += 1)
    {
        inst = instance_find(obj_slash_anim, i);
        with(inst) {
            instance_destroy();
        }
    }          
}
