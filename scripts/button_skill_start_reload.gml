var instance_action = argument0;

var i, inst;
show_debug_message("number of skill: " + string(instance_number(obj_skill_button)));
for (i = 0; i < instance_number(obj_skill_button); i += 1)
{
   inst = instance_find(obj_skill_button,i);
   if (inst.action == instance_action) {
    inst.state = SKILL_RELOAD;
    break;
   }
}
