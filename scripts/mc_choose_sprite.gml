/// Choose the sprite for mc skill
if (state == "pre_use_skill") {
    if (mc_is_using_skill == 1 && mc_promotion == 2)
        sprite_index = spr_mc2_skill_1_pre_attack;
    if (mc_is_using_skill == 1 && mc_promotion >= 3)
        sprite_index = spr_mc3_skill_1_pre_attack;        
        
    real_image_speed = 1;
} else if (state == "use_skill") {
   if (mc_is_using_skill == 1 && mc_promotion == 2)
        sprite_index = spr_mc2_skill_1_attack;
   else if (mc_is_using_skill == 1 && mc_promotion >= 3)
        sprite_index = spr_mc3_skill_1_attack;   
   else if (mc_is_using_skill == 2 && mc_promotion >= 3)
        sprite_index = spr_mc3_skill_2_attack;   

   real_image_speed = 0.5;
} else if (state == "rest" || state == "move" || state == "prepare_room") {
    if (mc_promotion == 1)
        sprite_index = spr_mc;
    else if (mc_promotion == 2)
        sprite_index = spr_mc2;
    else if (mc_promotion >= 3)        
        sprite_index = spr_mc3;    
} else if (state == "pre_attack") {
    if (mc_promotion == 1)
        sprite_index = spr_mc_pre_attack;
    else if (mc_promotion == 2)
        sprite_index = spr_mc2_pre_attack;
    else if (mc_promotion >= 3)
        sprite_index = spr_mc3_pre_attack;
                
    real_image_speed = 1;
} else if (state == "attack") {
    if (mc_promotion == 1)
        sprite_index = spr_mc_attack;
    else if (mc_promotion == 2)
        sprite_index = spr_mc2_attack;
    else if (mc_promotion >= 3)
        sprite_index = spr_mc3_attack;        

    real_image_speed = 0.5;        
}
