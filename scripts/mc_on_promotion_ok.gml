mc_promotion++;
obj_controller.difficult_level ++;

if (mc_promotion == 2) {
    unlock_skill(1);
} else if (mc_promotion == 3) {
    unlock_skill(2);
}  else if (mc_promotion == 4) {
    unlock_skill(3);
}

mc_change_state_to_rest();

with (other) {
    instance_destroy();
}

// Reset
global.timeScale = 1;

// promotion
promotion_start_time = 0;
position_effect_start_time = 0;
skill_explain_start_time = 0;
