///Create rain Particle

if (part_particles_count(partRain_sys) == 0) {
    // Rain system
    partRain_sys = part_system_create();
    
    // Rain Particle
    partRain = part_type_create();
    part_type_sprite(partRain, spr_blood_alphabet1, false, false, true);
    part_type_size(partRain, 1, 1, 0, 0);
    part_type_alpha2(partRain, 1, 1);
    part_type_gravity(partRain, 0.1, 290);
    part_type_speed(partRain, 0.2, 0.2, 0, 0);
    part_type_direction(partRain, 250, 330, 0, 0);
    part_type_orientation(partRain, 290, 290, 0, 0, 0);
    part_type_life(partRain, 20, 180);
    
    // Puddle Pảticle
    /*
    partPuddle = part_type_create();
    part_type_shape(partPuddle, pt_shape_circle);
    part_type_size(partPuddle, 0.5, 0.8, 0.01, 0);
    part_type_scale(partPuddle, 0.5, 0.1);
    part_type_color1(partPuddle, c_silver);
    part_type_alpha2(partPuddle, 0.4, 0);
    part_type_speed(partPuddle, 0, 0, 0, 0);
    part_type_direction(partPuddle, 0, 0, 0, 0);
    part_type_gravity(partPuddle, 0, 270);
    part_type_life(partPuddle, 50, 60);
    
    // Set Sequence
    part_type_death(partRain, 1, partPuddle);
    */
    
    // Create Emitter
    partRain_emit = part_emitter_create(partRain_sys);
    part_emitter_region(partRain_sys, partRain_emit, -400, view_wview, view_yview - 100, view_yview - 100, ps_shape_line, ps_distr_linear);
    part_emitter_stream(partRain_sys, partRain_emit, partRain, 5);

}
