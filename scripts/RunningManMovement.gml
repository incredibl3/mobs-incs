//Random move to left/right or top/bottom side of the room
b_move_vertical = irandom(1);

if(b_move_vertical == 1){
    //random move to left/right
    xPoint = irandom(1) * view_wview;
    if(x > xPoint)  image_xscale = 1;
    else image_xscale = -1;   
    real_speed = global.mob_runningman_movement_speed;
    move_towards_point(xPoint, random_range(0, view_hview), real_speed * global.timeScale);
}else{
    //random move to top/bottom
    xPoint = random_range(0, view_wview);
    if(x > xPoint)  image_xscale = 1;
    else image_xscale = -1;
    real_speed = global.mob_runningman_movement_speed;
    move_towards_point(xPoint, irandom(1) * view_hview, real_speed * global.timeScale);
}
